<?php
class message {

    const limit_username = 3;
    const limit_message = 10;
    private $username;
    private $message;

    public function __construct(string $username, string $message, ?DateTime $date = null)
    {
        $this->username = $username;
        $this->message = $message;
        $this->date = $date ?: new DateTime();

    }

    public function isValid():bool
    {
        return empty($this->getErrors());
    }

    public function getErrors(): array
    {
        $errors = [];
        if (strlen($this->username) < self::limit_username) {
            $errors['username'] = 'votre pseudo est tros court';
        }
        if (strlen($this->message) < self::limit_message) {
            $errors['message'] = 'votre message est tros court';
        }
        return $errors;
    }

    public function toHTML(): string
    {
        $username = htmlentities($this->username);
        $date = $this->date->format('d/m/Y à H:i');
        $message = htmlentities($this->message);
        return <<<HTML
        <p>
            <strong>{$username}</strong> <em>{$date}</em><br>
            {$message}
        </p>
HTML;
    }

    public function toJSON():string
    {
        return json_encode([
            'username' => $this->username,
            'message' => $this->message,
            'date' => $this->date->getTimestamp()
        ]);
    }
}
